<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

	}

	public function addstud()
	{
		$this->load->view('template/header');
		$this->load->view('student/adding');
		$this->load->view('template/footer');
	}
	
	
}
